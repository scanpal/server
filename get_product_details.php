<?php
 
 /**
 * \file get_product_details.php
 * \brief Following code will get single product details. 
 * A product is identified by product title (title)
 *
 */
 
// array for JSON response
$response = array();
$title = 0;
$queryItems = 0;


// include db connect class
require_once __DIR__ . '/db_connect.php';
require_once __DIR__ . '/jsonError.php';
require_once __DIR__ . '/scrapingFunctions.php';
 
//connecting to db
$db = new DB_CONNECT();

$barcodeNumber = $_GET["barcodeNumber"]; /**< Extracted barcode from the HTTP Get request */
$searchTitle = $_GET["searchTitle"]; /**< Extracted title from the HTTP Get request */


if ($barcodeNumber) 
{
 //save title
 //query items table to get details esp. item title for later query
 $queryItems = "SELECT * FROM item WHERE barcodeNumber = '$barcodeNumber'";
 $itemResult = mysql_query($queryItems);
 $title = mysql_result($itemResult,$row,"item_title");
}
else if ($searchTitle) 
{ 
	$title = $searchTitle;
	$queryItems = "SELECT * FROM item WHERE item_title = '$searchTitle'";
}
else
{
	echo "Not a valid barcode number";
	return;
}

if($title)
{
 //Search the DB for the data.
 $jsonData = "";
 $itemResult = mysql_query($queryItems);
 if(mysql_num_rows($itemResult) > 0) {
	 $rating = mysql_result($itemResult,$row,"rating");
	 $description = mysql_result($itemResult,$row,"description");
	 $image = mysql_result($itemResult,$row,"image");
	 $title = mysql_result($itemResult,$row,"item_title");
}
// Otherwise scrape the data needed to create the item
else
{


		 $scrapedData = scrapeBasicInfo($title);
		 
		 //echo "ScrapedData <br>";
		 // Put all the data into a single string
		 $i = 0;
		 foreach ($scrapedData as &$value) {
		 	// console
		 	if ($i == 0)
		 	{
		 		$value[1] = '"';
		 		$value[9] = '"';
		 		$value[12] = '"';
		 		$value[strlen($value)-2] = '"';
		 	}
		 	// description
		 	else if ($i == 1)
		 	{
		 		// Might need to change all " to ' first
		 		$value = str_replace('"', "'" ,$value);
		 		
		 		$value[1] = '"';
		 		$value[13] = '"';
		 		$value[16] = '"';
		 		$value[strlen($value)-2] = '"';
		 	}
		 	// image
		 	else if ($i == 2)
		 	{
		 		$value[1] = '"';
		 		$value[7] = '"';
		 		$value[10] = '"';
		 		$value[strlen($value)-2] = '"';
		 	}
		 	// rating
		 	else if ($i == 3)
		 	{
		 		$value[1] = '"';
		 		$value[8] = '"';
		 		$value[11] = '"';
		 		$value[strlen($value)-2] = '"';
		 	}
		 	// title
		 	else if ($i == 4)
		 	{
		 		$value[1] = '"';
		 		$value[7] = '"';
		 		$value[10] = '"';
		 		$value[strlen($value)-2] = '"';
		 	}
		 	$i++;
		 	
			//echo $value;
			//echo "<br>";
			//echo ".\n.";
			$jsonData .= $value;
			//echo $value;
		}
		
		//echo "Jsondata";
		//echo $jsonData;
	}
 
 //use title to query reviews and videos
 $queryReviews = "SELECT url,review FROM reviews WHERE item_title = '$title'";
 $queryVideos = "SELECT encoded_ID,video_title FROM video where item_title = '$title'";
 $queryPrices = "SELECT price,store_url,platform FROM priceItem where item_title = '$title'";
  
 //results of queries
 $reviewResult = mysql_query($queryReviews);
 $vidResult = mysql_query($queryVideos);
 $priceResult = mysql_query($queryPrices);
 
 //define row sizes
 $reviewnum=mysql_num_rows($reviewResult);
 $videonum=mysql_num_rows($vidResult);
 $pricenum=mysql_num_rows($priceResult);

//get data from item query array
    if (!empty($itemResult)) 
	{
        // check for empty result 
        if (mysql_num_rows($itemResult) > 0 || $jsonData != "" ) {
 
			// Check to see if the data has been scraped
			if($jsonData != "")
			{
				// Create an array for the information to go into
				$product = array();
				$product = json_decode($jsonData, true);
				
				// Add the data into the array
				$response["product"] = array();
				array_push($response["product"], $product);
				//echo $response["product"];
				
				// success
				$response["success"] = 1;
			}
			
			// Otherwise it has been loaded in from the DB
			else{
				// Set up the JSON data
				$product = array();
				$product["title"] = $title;
				$product["rating"] = $rating;
				
				//convert text block to utf-8
				$description = mb_convert_encoding($description,'UTF-8','UTF-8');
				$description = json_encode($description);
				
				$product["description"] = $description;
				$product["image"] = $image;

				// user node
				$response["product"] = array();
				array_push($response["product"], $product);
				
				// success
				$response["success"] = 1;
			}
			
			// Try and retrieve the review data from the DB
			if($reviewnum != 0)
			{
				// Get review data
				$response["reviews"] = array();
				for($i = 0; $i < $reviewnum; $i++)
				{
					$review = array();
					$reviewURL = mysql_result($reviewResult,$i,"url");
					$reviewTEXT = mysql_result($reviewResult,$i,"review");
					
					//convert text block to utf-8
					$reviewTEXT = mb_convert_encoding($reviewTEXT,'UTF-8','UTF-8');
					$reviewTEXT = json_encode($reviewTEXT);

					$review["reviewurl"] = $reviewURL;
					$review["review"] = $reviewTEXT;
					
					
h($response["reviews"], $review);
				}
			
			}
			
			// No review data in the DB. Scrape it.
			else
			{
				$scrapedData = scrapeReview($title);

				// Create an array for the information to go into
				$review = array();
				$review = json_decode($scrapedData, true);
				
				$response["reviews"] = array();
				$tempstring = "";
				$i = 0;
				foreach ($scrapedData as &$r)
				{
					
					//echo "<br>";
					if( $i == 0 )
					{
						// change
						// {'console': 'pc',
						// to
						// {"console": "pc",
						$r = str_replace("'", '"', $r);
			
					}
					else if ( $i == 1 )
					{
						// change
						// 'review': 'The hundreds of beautifully rendered monstrosities, items and labyrinths will keep you on your toes for months. Blizzard is also one of the first software companies to offer FREE MULTI-PLAYER GAMING.',
						// to have double quotes
						$r[1] = '"';
						$r[8] = '"';
						$r[11] = '"';
						$r[strlen($r)-2] = '"';
					
					
					}
					
					else
					{
						// 'url': 'http://www.game-revolution.com/games/pc/rpg/diablo.htm'}
						
						$r = str_replace("'", '"', $r);
					
					}
					
					// Add the to the string that will be decoded
					$tempstring .= $r;
					
					$i++;
					
					// Check if we have all three components
					if ($i == 3)
					{
						$i = 0;
						$temparray = array();
						$temparray = json_decode($tempstring, true);
						$tempstring = "";
						
						// Save the strings into an array
						$review = array();
						$j = 0;
						
						
						$reviewsizecheck = true;	// This will be set to false if the 
													// strings are to small. i.e. invalid data
													
						foreach ($temparray as &$t)
						{
							if ($j == 1)
							{
								$review["review"] = $t;
								if (strlen($t) < 20 )
								{
									$reviewsizecheck = false;
								}
							}
							else if ($j == 2)
							{
								$review["reviewurl"] = $t;
								if (strlen($t) < 10 )
								{
									$reviewsizecheck = false;
								}
							}
							
							$j++;
							
							if ($j == 3)
							{
								$j = 0;
								if($reviewsizecheck == true)
								{
									//array_push($response["reviews"], $review);
								}
								$reviewsizecheck = true;
							}	
						}
					}
				}
			}

			// Try to load in the video data from the DB
			if($videonum != 0)
			{
				// Get video data
				$response["videos"] = array();
				for($i = 0; $i < $videonum; $i++)
				{
					$video = array();
					$videoId = mysql_result($vidResult,$i,"encoded_ID");
					$videoTitle = mysql_result($vidResult,$i,"video_title");
					
					//convert text block to utf-8
					$videoTitle = mb_convert_encoding($videoTitle,'UTF-8','UTF-8');
					$videoTitle = json_encode($videoTitle);
					
					$video["videoid"] = $videoId;
					$video["videotitle"] = $videoTitle;
					
					
					array_push($response["videos"], $video);
					
				}
			}
			
			// Scrape the video data
			else
			{
				//echo "Invalid video result\n";
				//echo "<br>videoScraping <br>";
				$response["videos"] = array();
				$videoData = scrapeVideo($title);
				
				$j = 0;
				$tempstring = "";
				$formated = false;
				foreach ($videoData as &$r)
				{
					
					// --- Extract the data and fix the formatting for json decoding ---
					// Check if there review is spaced over one line
					if (strpos($r,'i') == 2 && strpos($r,'}') == (strlen($r)-1))
					{
						// Index location of the comma
						$commapos =  strrpos($r, ",", 0);
						$r[1] = '"';
						$r[4] = '"';
						$r[7] = '"';
						$r[$commapos-1] = '"';
						
						$r[$commapos+2] = '"';
						$r[$commapos+8] = '"';
						$r[$commapos+11] = '"';
						$r[strlen($r)-2] = '"';
						$j = 0;
						$tempstring = $r;
						$formated = true;
					}	 
				
					// id
					else if ($j == 0)
					{
						$j++;
						$r[1] = '"';
						$r[4] = '"';
						$r[7] = '"';
						$r[strlen($r)-2] = '"';
						$tempstring .= $r;
					}
				
					// title
					else if ($j == 1)
					{
						$j++;
						$r[1] = '"';
						$r[7] = '"';
						$r[10] = '"';
						$r[strlen($r)-2] = '"';
						$tempstring .= $r;
						$j = 0;
						$tempstring = "";
						$formated = true;
					}
					
					// --- Decode the data and add it to the array ---
					if($formated == true)
					{
						$temparray = array();
						$temparray = json_decode($tempstring, true);
					
						$i = 0;
						
						$reviewsizecheck = true;	// This will be set to false if the 
													// strings are to small. i.e. invalid data
						foreach ($temparray as &$e)
						{
							if( $i == 0)
							{
								$i++;
								$video["videoid"] = $e;
								if (strlen($e) < 8)
								{
									$reviewsizecheck = false;
								}
							}
							else
							{
								$i=0;
								$video["videotitle"] = $e;
								if (strlen($e) < 5)
								{
									$reviewsizecheck = false;
								}
								if($reviewsizecheck == true)
								{
									array_push($response["videos"], $video);
								}
								$reviewsizecheck = true;
							}
						}
					}
				}
			}
			
			if(!empty($priceResult))
			{
				// Get price data
				$response["prices"] = array();
				for($i = 0; $i < $pricenum; $i++)
				{
					$price = array();
					
					// Get the data from the table
					$storeUrl = mysql_result($priceResult,$i,"store_url");
					$itemPrice = mysql_result($priceResult,$i,"price");
					$platform = mysql_result($priceResult,$i,"platform");
					
					// Save it in the temp array
					$price["storeurl"] = $storeUrl;
					$price["price"] = $itemPrice;
					$price["platform"] = $platform;		
					array_push($response["prices"], $price);
				}
			}
			else
			{
				echo "Invalid price result";
			}
			
			if($reviewResult && $vidResult && $priceResult)
			{
				// echoing JSON response
				echo json_encode($response);
			}
			

        } else {
       
        
        
            // no product found
            $response["success"] = 0;
            $response["message"] = "No product found 1\n";
 
            // echo no users JSON
            echo json_encode($response);
            return;
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No product found 2\n";
 
        // echo no users JSON
        echo json_encode($response);
        return;
    }
	$title = 0;
} 

//There is no title available in the database -- START SCRAPER -- 
else 
{
	$scrapedTitle = scrapeTitle($barcodeNumber);
	scrapeBasicInfo($scrapedTitle);
}

//check if json caused an error
if(json_last_error())
	jsonErrorCatch();
?>
