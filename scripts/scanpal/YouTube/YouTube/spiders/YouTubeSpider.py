from scrapy.spider import Spider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from YouTube.YouTubeItem import YouTubeItem
import re, sys, scrapy

# To use this spider use a command such as	
# scrapy crawl YouTubeSpider -a title='simcity 5'

global_title = ''					# holds the title of the game

class YouTubeSpider(Spider):
	name = "YouTubeSpider"
	allowed_domains = ["youtube.com"]
	start_urls = [""]
	
	# Initialization of the spider.
	# This gets the arguments that are posted to the spider
	def __init__(self,title='', *args, **kwargs):
		super(YouTubeSpider, self).__init__(*args, **kwargs)
		self.start_urls = [CreateURL(title)] 
	
	def parse(self,response):
		# Set up the selector
		sel = HtmlXPathSelector(response)
		
		# Create an array for all the video items
		items = []

		# Itterate though each video link
		for video in sel.select('//*[@id="results"]/ol/li/ol/li'):
		
			# Create a new empty item
			item = YouTubeItem()
			
			# Add in the title information
			temp_title = video.select('div/div[2]/h3/a/text()').extract()
			item['title'] = ParseString(str(temp_title))
			
			# Add in the video ID
			temp_id = video.select('div/div[2]/h3/a/@href').extract();
			# extract the id
			item['id'] = str(temp_id).replace("/watch?v=",'').replace("[u'",'').replace("']",'')
			
			# Send down the pipeline to the items file. Need to make the pipeline remove duplicates.
			items.append(item)
		
		# Write the items to a file
		WriteToFile(items)
		
		return items
		
# A function that takes an array of items and outputs them into a file
def WriteToFile (items):
	# Set up the output ile name
	log_title = global_title
	log_title += ".txt"

	# create the output file
	with open(log_title, 'a') as f:
		for item in items:
			f.write('title: {0}, id: {1}\n'.format(item['title'], item['id']))
		
# A function that creates/returns the YouTube url
def CreateURL(title):
	# save the title
	global global_title
	global_title = title
	
	# Generate the link based of the title
	new_title = title.replace(" ",'+')
	url = "https://www.youtube.com/results?search_query="
	url += new_title
	
	return url
		
# Parse the string so that it only contains the correct characters
def ParseString(_string):
	new_string = _string.replace("[u'",'').replace("']",'').replace('[u"','').replace('"]"','').replace('\"','"').replace('"]','')
	return new_string
