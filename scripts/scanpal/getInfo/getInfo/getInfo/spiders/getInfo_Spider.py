##	\file getInfo_Spider.py
#	\brief Defines a spider to crawl www.metacritic.com for information regarding an item.
#	@see http://www.metacritic.com
#	@see runBasicInfo.py


import re, sys, scrapy, roman
from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher 
from scrapy.item import Item, Field
from getInfo.items import getInfoItem

consoles = ['xbox','ps','playstation-2','wii','ds','gamecube','n64','gba','psp','dreamcast','playstation-4','xbox-one','playstation-3','xbox-360','pc','wii-u','3ds','playstation-vita','ios']
items = []
globalTitle = ""
originalTitle = ""

##	\brief Defines the getInfoSpider object
#
#
class getInfoSpider(Spider):

	##	@var name
	#	The name of the spider	
	name = "getInfoSpider" 
	
	##	@var allowed_domains
	#	The url of where the spider will be working
	allowed_domains = ["http://www.metacritic.com/game"]

	start_urls = ''
	
	##	\brief Initialises the getInfoSpider class
	#
	#	\param title	
	#		The title of the item to scrape the basic information for
	#	
	def __init__(self,title='', *args, **kwargs):
		super(getInfoSpider, self).__init__(*args, **kwargs)
		dispatcher.connect(self.spider_quit,signals.spider_closed)
		
		#converting to roman num.
		global originalTitle
		originalTitle = title
		copyTitle = title
		copyTitle = copyTitle.lower()
		title = convertNumber(title)
		global globalTitle
		globalTitle = title
		
		if not title == '0':
			title = title.lower()
						
				
		dispatcher.connect(self.spider_quit,signals.spider_closed)
		# Generate a list of all possible urls to try and load
		starturl = "http://www.metacritic.com/game/"
		global consoles
		urls = []
		for console in consoles:
			tempurl = starturl
			tempurl += console
			tempurl += '/'
			otherurl = tempurl
			if not title == '0':
				tempurl += title
				urls.append(tempurl)
			otherurl += copyTitle
			urls.append(otherurl)
			
		self.start_urls = urls
	
	##	\brief Parses the response into a JSON array
	#
	#	\return items
	#		Returns an array of items found.
	#
	#	
	def parse(self,response):
		# Set up the selector
		sel = Selector(response)
		
		# Get the platform
		console = GetConsoleFromUrl(response)
		
		item = getInfoItem()
		# Get the rating
		rating = sel.xpath('//*[@id="main"]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/a/div/span/text()').extract()
		
		
		# Check to make sure a rating exists. If it doesnt assume that there is no page contents for this platform
		if rating:
			rating = str(rating)[3:][:-2]
			
			# Get the description and format it
			description = sel.xpath('//*[@id="main"]/div/div[3]/div/div[2]/div[2]/div[1]/ul/li/span[2]/span/span[1]/text()').extract()
			descriptionsecondhalf = sel.xpath('//*[@id="main"]/div/div[3]/div/div[2]/div[2]/div[1]/ul/li/span[2]/span/span[2]/text()').extract()
			
			# Remove unwanted characters
			description = str(description)[3:][:-2]
			descriptionsecondhalf = str(descriptionsecondhalf)[3:][:-2]
			
			# Add the strings together
			description += descriptionsecondhalf
			#description = description.replace("'", '"')
		
			
			# Get the image url and format it
			image = sel.xpath('//*[@id="main"]/div/div[3]/div/div[1]/div[1]/img/@src').extract()
			image = str(image)[3:][:-2]
			
			# setup the item
			item['title'] = originalTitle
			item['rating'] = rating
			item['description'] = description 
			item['image'] = image
			item['console'] = console
			
			items.append(item) 
			return 
	
	##	\brief Destructor for spider - exits it from event loop. 
	#
	#	Before exiting the event loop the spider returns the largest item
	#	in the array to display to the front end.
	#
	#		
	def spider_quit(self,spider):
		#find biggest item based on length of array
		size = 0
		largest = -1
		count = -1
		for index in range(len(items)):
			count = count + 1
			if sys.getsizeof(items[index]) > size:
				size = sys.getsizeof(items[index])
				largest = count
				returnItem = items[count]
				
		print str(returnItem)


##	\brief Gets a console from specified url
#
#	\param url
#		The URL of the item to find the console for
#	\return console
#		The console in string format
#
def GetConsoleFromUrl(url):
	urlstring = str(url)
	global consoles
	for console in consoles:
		if console in urlstring:
			return console
			
##	\brief Converts a number to roman numeral for searching purposes.
#	As some items in a sequence (e.g. Civilization 5) have their urls stored as the roman
#	numeral, if the item is not found the number is converted to the roman equivalent.
#
#	\param title
#		The title to have the number converted
#	\return console
#		The new parsed title with roman numeral
#
def convertNumber(title):
	temp = -1
	pos = -1
	first = True
	newTitle = ""
	for i in reversed(title):
		temp = temp + 1
		if i == '-':
			if first:
				revTitle= title[::-1]
				newTitle = revTitle[0:temp]
				first = False
				pos = temp
			
	newTitle = newTitle[::-1]
	
	if not newTitle.isdigit():
		return "0"
	
	numbers = roman.toRoman(int(newTitle))
	title = title[0:(title.__len__()-pos)]
	title = title + numbers
	return title
