# Scrapy settings for basicInfo project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'getInfoSpider'

SPIDER_MODULES = ['getInfo.spiders']
NEWSPIDER_MODULE = 'getInfo.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'basicInfo (+http://www.yourdomain.com)'
