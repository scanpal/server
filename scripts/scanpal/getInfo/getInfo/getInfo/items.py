# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html


import scrapy.item

class getInfoItem(scrapy.item.Item):
	# define the fields for your item here like:
	title = scrapy.item.Field()
	description = scrapy.item.Field()
	rating = scrapy.item.Field()
	image = scrapy.item.Field()
	console = scrapy.item.Field()
	pass
	
