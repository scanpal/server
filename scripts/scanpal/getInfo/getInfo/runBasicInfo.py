##	\file runBasicInfo.py
#	\brief Calls the basic info spiderusing the Twisted Reactor.
#	@see getInfo_Spider.py
import sys, os

from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy import log, signals
from getInfo.spiders.getInfo_Spider import getInfoSpider
from scrapy.utils.project import get_project_settings

theTitle = str(sys.argv[1])
spider = getInfoSpider(title=theTitle)
settings = get_project_settings()
crawler = Crawler(settings)
crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
crawler.configure()
crawler.crawl(spider)
crawler.start()	

log.start(logfile="results.log", loglevel=log.DEBUG, crawler=crawler, logstdout=False)

reactor.run() #the script will block here until spider_closed signal

