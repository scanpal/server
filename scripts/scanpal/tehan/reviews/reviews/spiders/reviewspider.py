from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from reviews.items import ReviewsItem
import re, sys, scrapy

consoles = ['xbox','ps','playstation-2','wii','ds','gamecube','n64','gba','psp','dreamcast','playstation-4','xbox-one','playstation-3','xbox-360','pc','wii-u','3ds','playstation-vita','ios']
	

class reviewsspider(Spider):
	name = "reviewsspider"
	allowed_domains = ["http://www.metacritic.com/game"]
	start_urls = ''
	
	def __init__(self,title='', *args, **kwargs):
		super(reviewsspider, self).__init__(*args, **kwargs)
		
		title = title.replace(' ','-')
		
		# Generate a list of all possible urls to try and load
		starturl = "http://www.metacritic.com/game/"
		global consoles
		endurl = "/critic-reviews"
		urls = []
		for console in consoles:
			tempurl = starturl
			tempurl += console
			tempurl += '/'
			tempurl += title
			tempurl += endurl
			urls.append(tempurl)
		self.start_urls = urls
	
	def parse(self,response):
		# Set up the selector
		sel = Selector(response)
		
		# Get the platform
		console = GetConsoleFromUrl(response)
		
		# Create an array for all the video items
		items = []
		
		# Get the parent node from which all the reviews are in
		parent = sel.xpath('//*[@id="main"]/div[5]/div/ol/li')
		
		# Itterate though each node and extract the data
		for reviews in parent:

			item = ReviewsItem()
			
			# Get the url then format it
			url = reviews.xpath('div/div/div/div/div/div[2]/ul/li[2]/a/@href').extract()
			newurl = str(url)[3:][:-2]
			
			# Get the review and then format it
			review = reviews.xpath('div/div/div/div/div/div[1]/div[2]/text()').extract()
			newreview = str(review)[41:]
			newreview = newreview[:-36]
			
			# setup the item
			# check to make sure there is a url for valid data
			if newurl.startswith('http'):
				item['url'] = newurl
				item['review'] = newreview
				item['console'] = console
				items.append(item)
			
		WriteToFile(items)
		return items


def WriteToFile (items):
	# Set up the output ile name
	log_title = "test"
	log_title += ".txt"

	# create the output file
	with open(log_title, 'a') as f:
		for item in items:
			f.write('url: {0}\nconsole: {1} \nreview: {2}\n\n'.format(item['url'], item['console'], item['review']))
	
# Get the console from the url
def GetConsoleFromUrl(url):
	urlstring = str(url)
	global consoles
	for console in consoles:
		if console in urlstring:
			return console
	
	
