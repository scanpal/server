import re, sys, scrapy
from scrapy.spider import Spider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from basicinfo.items import BasicinfoItem


consoles = ['xbox','ps','playstation-2','wii','ds','gamecube','n64','gba','psp','dreamcast','playstation-4','xbox-one','playstation-3','xbox-360','pc','wii-u','3ds','playstation-vita','ios']
	
class BasicInfoSpider(Spider):
	print "spider called"
	name = "basicinfospider"
	allowed_domains = ["http://www.metacritic.com/game"]
	start_urls = ''
	
	def __init__(self,title='', *args, **kwargs):
		super(BasicInfoSpider, self).__init__(*args, **kwargs)
		
		title = title.replace(' ','-')
		
		# Generate a list of all possible urls to try and load
		starturl = "http://www.metacritic.com/game/"
		global consoles
		urls = []
		for console in consoles:
			tempurl = starturl
			tempurl += console
			tempurl += '/'
			tempurl += title
			urls.append(tempurl)
		self.start_urls = urls
	
	def parse(self,response):
		# Set up the selector
		sel = HtmlXPathSelector(response)
		
		# Get the platform
		console = GetConsoleFromUrl(response)
		
		item = BasicinfoItem()
		print "item created"
		# Get the rating
		rating = sel.select('//*[@id="main"]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/a/div/span/text()').extract()
		
		# Check to make sure a rating exists. If it doesnt assume that there is no page contents for this platform
		if rating:
			rating = str(rating)[3:][:-2]
			
			# Get the description and format it
			description = sel.select('//*[@id="main"]/div/div[3]/div/div[2]/div[2]/div[1]/ul/li/span[2]/span/span[1]/text()').extract()
			desctiptionsecondhalf = sel.select('//*[@id="main"]/div/div[3]/div/div[2]/div[2]/div[1]/ul/li/span[2]/span/span[2]/text()').extract()
			description += desctiptionsecondhalf
			description = str(description)[3:][:-2]
			
			# Get the image url and format it
			image = sel.select('//*[@id="main"]/div/div[3]/div/div[1]/div[1]/img/@src').extract()
			image = str(image)[3:][:-2]
			
			# setup the item
			item['rating'] = rating
			item['description'] = description 
			item['image'] = image
			item['console'] = console
			
			print rating
			print description
			print image
			print console
				
			#WriteToFile(item)
			return item


def WriteToFile (item):
	# Set up the output ile name
	log_title = "test"
	log_title += ".txt"

	# create the output file
	with open(log_title, 'a') as f:
		f.write('rating: {0}\nconsole: {1}\nimage: {2}\ndescription: {3}\n\n'.format(item['rating'], item['console'], item['image'],item['description']))
	
# Get the console from the url
def GetConsoleFromUrl(url):
	urlstring = str(url)
	global consoles
	for console in consoles:
		if console in urlstring:
			return console
	
	
