##	\file getBarcode.py
#	\brief Defines a spider that gets a title from a barcode from Fishpond	

from scrapy import log # This module is useful for printing out debug information
from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request

##	\brief Defines a getBarcode spider object
class getBarcode(Spider):
	name = "getBarcode"
	allowed_domains = ["fishpond.com.au"]
	start_urls = [""]
	
	## \brief Initialises the spider. 
	#
	# \param barcode 
	#		The barcode passed in from the runBarcode.py script
	def __init__(self, barcode='', *args, **kwargs):
		super(getBarcode, self).__init__(*args, **kwargs)
		
		# Construct the url
		url = "http://www.fishpond.com.au/q/"
		url += barcode
		self.start_urls = [url] 
		
	##	\brief Parses the gathered title into a string
	#
	#	\return title
	#		Title linked to barcode number gathered from www.fishpond.com.au
	#	
	def parse(self,response):
		# Setup the selector
		sel = Selector(response)
		
		# Get the title
		title = sel.xpath('//*[@id="product_title"]/span[1]/text()').extract()
		print str(title)
