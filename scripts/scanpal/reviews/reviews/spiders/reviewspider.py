##	\file reviewspider.py
#	\brief Defines a spider to crawl www.metacritic.com for reviews for an item
#	@see http://www.metacritic.com
#	@see runReviews.py



from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from reviews.items import ReviewsItem
import re, sys, scrapy, roman

consoles = ['xbox','ps','playstation-2','wii','ds','gamecube','n64','gba','psp','dreamcast','playstation-4','xbox-one','playstation-3','xbox-360','pc','wii-u','3ds','playstation-vita','ios']
	
## \brief Defines a reviewsspider object
class reviewsspider(Spider):
	
	##	@var name
	#	The name of the spider	
	name = "reviewsspider"
	
	##	@var allowed_domains
	#	The url of where the spider will be working	
	allowed_domains = ["http://www.metacritic.com/game"]
	start_urls = ''
	
	##	\brief Initialises the reviewsspider class
	#
	#	\param title	
	#		The title of the item to scrape the reviews for
	#	
	def __init__(self,title='', *args, **kwargs):
		super(reviewsspider, self).__init__(*args, **kwargs)
		
		title = title.replace(' ','-')
		
		# Generate a list of all possible urls to try and load
		starturl = "http://www.metacritic.com/game/"
		global consoles
		endurl = "/critic-reviews"
		urls = []
		for console in consoles:
			tempurl = starturl
			tempurl += console
			tempurl += '/'
			tempurlroman = tempurl
			tempurl += title
			tempurlroman += convertNumber(title)
			tempurl += endurl
			tempurlroman += endurl
			urls.append(tempurl)
			urls.append(tempurlroman)
		self.start_urls = urls
		
		
	##	\brief Parses the response into a JSON array
	#
	#	\return items
	#		Returns an array of reviews found.
	#
	#	
	def parse(self,response):
		# Set up the selector
		sel = Selector(response)
		
		# Get the platform
		console = GetConsoleFromUrl(response)
		
		# Create an array for all the video items
		items = []
		
		# Get the parent node from which all the reviews are in
		parent = sel.xpath('//*[@id="main"]/div[5]/div/ol/li')
		
		# Itterate though each node and extract the data
		for reviews in parent:
			item = ReviewsItem()
			
			# Get the url then format it
			url = reviews.xpath('div/div/div/div/div/div[2]/ul/li[2]/a/@href').extract()
			newurl = str(url)[3:][:-2]
			
			# Get the review and then format it
			review = reviews.xpath('div/div/div/div/div/div[1]/div[2]/text()').extract()
			newreview = str(review)[41:]
			newreview = newreview[:-36]
			
			# setup the item
			# check to make sure there is a url for valid data
			item["url"] = newurl
			item["review"] = newreview
			item["console"] = console
			items.append(item)
			
		for item in items:
			if not str(item) == "[]":
				print str(item)
		

##	\brief Gets a console from specified url
#
#	\param url
#		The URL of the item to find the console for
#	\return console
#		The console in string format
#
def GetConsoleFromUrl(url):
	urlstring = str(url)
	global consoles
	for console in consoles:
		if console in urlstring:
			return console
			
def convertNumber(title):
	temp = -1
	pos = -1
	first = True
	newTitle = ""
	for i in reversed(title):
		temp = temp + 1
		if i == '-':
			if first:
				revTitle= title[::-1]
				newTitle = revTitle[0:temp]
				first = False
				pos = temp
			
	newTitle = newTitle[::-1]
	
	if not newTitle.isdigit():
		return "0"
	
	numbers = roman.toRoman(int(newTitle))
	title = title[0:(title.__len__()-pos)]
	title = title + numbers
	return title.lower()
