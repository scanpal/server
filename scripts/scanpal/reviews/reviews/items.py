# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ReviewsItem(scrapy.item.Item):
    url = scrapy.item.Field()
    review = scrapy.item.Field()
    console = scrapy.item.Field()
    pass
