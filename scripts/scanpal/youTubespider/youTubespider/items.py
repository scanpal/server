import scrapy

class YouTubeItem(scrapy.item.Item):
    title = scrapy.item.Field()
    id = scrapy.item.Field()
    pass
