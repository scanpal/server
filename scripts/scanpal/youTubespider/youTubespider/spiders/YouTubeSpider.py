##	\file YouTubeSpider.py
#	\brief Defines a spider to search www.youtube.com for videos regarding an item
#	@see http://www.youtube.com
#	@see runYoutube.py


from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from youTubespider.YouTubeItem import YouTubeItem
import re, sys, scrapy

# To use this spider use a command such as	
# scrapy crawl YouTubeSpider -a title='simcity 5'

global_title = ''					# holds the title of the game

class YouTubeSpider(Spider):
	name = "youTubespider"
	allowed_domains = ["youtube.com"]
	start_urls = [""]
	
	
	##	\brief Initialises the YouTubeSpider class
	#
	#	\param title	
	#		The title of the item to scrape the videos for
	#	
	def __init__(self,title='', *args, **kwargs):
		super(YouTubeSpider, self).__init__(*args, **kwargs)
		self.start_urls = [CreateURL(title)] 
	
	##	\brief Parses the response into a JSON array
	#
	#	\return items
	#		Returns an array of videos found.
	#
	#
	def parse(self,response):
		# Set up the selector
		sel = Selector(response)
		
		# Create an array for all the video items
		items = []

		# Itterate though each video link
		for video in sel.xpath('//*[@id="results"]/ol/li/ol/li'):
		
			# Create a new empty item
			item = YouTubeItem()
			
			# Add in the title information
			temp_title = video.xpath('div/div[2]/h3/a/text()').extract()
			item['title'] = ParseString(str(temp_title))
			
			# Add in the video ID
			temp_id = video.xpath('div/div[2]/h3/a/@href').extract();
			# extract the id
			item['id'] = str(temp_id).replace("/watch?v=",'').replace("[u'",'').replace("']",'')
			
			# Send down the pipeline to the items file. Need to make the pipeline remove duplicates.
			items.append(item)
			
		for item in items:
			if not str(item) == "[]":
				print str(item)
		
##	\brief A function that creates/returns the YouTube url
#
#	\return url
#		Returns the required url
#
#
def CreateURL(title):
	# save the title
	global global_title
	global_title = title
	
	# Generate the link based of the title
	new_title = title.replace(" ",'+')
	url = "https://www.youtube.com/results?search_query="
	url += new_title
	
	return url
		
#
##	\brief A function that will parse the string so that it only contains the correct characters
#
#	\return new_string
#		Returns the parsed string
#
#
def ParseString(_string):
	new_string = _string.replace("[u'",'').replace("']",'').replace('[u"','').replace('"]"','').replace('\"','"').replace('"]','')
	return new_string
