# -*- coding: utf-8 -*-

# Scrapy settings for YouTube project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'youTubespider'

SPIDER_MODULES = ['youTubespider.spiders']
NEWSPIDER_MODULE = 'youTubespider.spiders'
DEPTH_LIMIT = 1								# Restrict the limit to 1 page
DOWNLOAD_DELAY = .5							# Download delay, slows the page requests as to not be blocked or booted
ITEM_PIPELINES = {
    'youTubespider.pipelines.YoutubePipeline': 300,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'YouTube (+http://www.yourdomain.com)'
