##	\file runYoutube.py
#	\brief Calls the spider using the Twisted Reactor.
# 	@see YouTubeSpider.py	


import sys, os

from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy import log, signals
from youTubespider.spiders.YouTubeSpider import YouTubeSpider
from scrapy.utils.project import get_project_settings


titleArg = str(sys.argv[1])
spider = YouTubeSpider(title=titleArg)
settings = get_project_settings()
crawler = Crawler(settings)
crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
crawler.configure()
crawler.crawl(spider)
crawler.start()
	
log.start(logfile="results.log", loglevel=log.DEBUG, crawler=crawler, logstdout=False)

reactor.run() #the script will block here until spider_closed signal



