<?php
/** \file jsonError.php
 *  \brief This code defines an error catch function to detect any errors in the JSON formatting. It converts the obscure code to the definition as provided in the JSON documentation.
 *  @see http://php.net/manual/en/function.json-last-error.php
 *
 */
 function jsonErrorCatch() {
	switch (json_last_error())
	{		
		//JSON_ERROR_DEPTH
		case 1:
		echo "\nMaximum stack depth exceeded\n";
		break;
		
		//JSON_ERROR_STATE_MISMATCH
		case 2:
		echo "\nInvalid JSON\n";
		break;
		
		//JSON_ERROR_CTRL_CHAR
		case 3:
		echo "\nIncorrect encoding\n";
		break;
		
		//JSON_ERROR_SYNTAX
		case 4:
		echo "\nSyntax error\n";
		break;
		
		//JSON_ERROR_UTF8
		case 5:
		echo "\nMalformed UTF-8\n";
		break;
		
		//JSON_ERROR_RECURSION
		case 6:
		echo "\nRecursion error\n";
		break;
		
		//JSON_ERROR_INF_OR_NAN
		case 7:
		echo "\nNaN or INF somewhere\n";
		break;
		
		//JSON_ERROR_UNSUPPORTED_TYPE
		case 8:
		echo "\nUnsupported type\n";
		break;
	}
 }
 
?>