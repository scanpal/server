<?php


/** \file scrapingFunctions.php
 *	\brief This code defines all functions used to call the scraping framework 
 *
 *
 *
 */

/** scrapeTitle($barcodeNumber)
 * 
 * @param $barcodeNumber
 *		barcode number of item to be searched for
 *
 * @return $title
 *		returns title scraped using scrapy
 */
function scrapeTitle($barcodeNumber) {

$output = 0;

// Set up the python command
$cmd = "python /etc/apache2/sites-available/scripts/scanpal/Fishpond/getBarcode/runBarcode.py 		barcode=";
$cmd .= (string)$barcodeNumber;


// Execute the command
exec($cmd, $output);


// Fix up the solution so its just the title
$title = $output[0];
$title = substr($title,3,-2);
				
return $title;
}



/** scrapeBasicInfo($title)
 *
 * @param $title
 *		title of item to find basic info for
 *
 * @return $output
 *		returns array of information to script for processing. includes rating, description,  
 * 		console and image
 *
 */
function scrapeBasicInfo($title) {

$output = 0;

$title = str_replace(' ','-',$title);
$title = strtolower($title);

// Now for basic info
$cmd = "python /etc/apache2/sites-available/scripts/scanpal/getInfo/getInfo/runBasicInfo.py ";
$cmd.= (string)$title;
$outputs = exec($cmd, $output, $return);

return $output;

}

/** scrapeVideo($title)
 *
 * @param $title
 *		title of item to find video url for
 *
 * @return $output
 *		returns unique identifier to link to YouTube video related to the item title
 *
 */
function scrapeVideo($title) {

$cmd = "python /etc/apache2/sites-available/scripts/scanpal/youTubespider/runYoutube.py ";
$cmd .= (string)$title;
$outputs = exec($cmd,$output,$return);

return $output;
}

/** scrapeReview($title)
 *
 * @param $title
 *		title of item to find reviews for
 *
 * @return $output
 *		returns array of reviews to script
 *
 */
function scrapeReview($title) {

$cmd = "python /etc/apache2/sites-available/scripts/scanpal/reviews/runReviews.py ";
$cmd .= (string)$title;

$outputs = exec($cmd,$output,$return);

return $output;


}



?>
